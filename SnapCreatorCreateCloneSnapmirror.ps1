Param (
    [Parameter(Mandatory=$True)]
    [String]$profile,
	
    [Parameter(Mandatory=$True)]
    [String]$config,

	[Parameter(Mandatory=$True)]
	[String]$clonename,		

	[Parameter(Mandatory=$False)]
	[String]$schedule='daily',	
	
	[Parameter(Mandatory=$False)]
	[String]$scpasswd	
)

$sc = 1; $na = 1;
$PSScriptRoot = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$GlobalFile = $PSScriptRoot+'\SCGlobalConfig.ps1'
if (!([System.IO.File]::Exists($GlobalFile))) {
   Write-Host "ERROR: failed to locate required file $($PSScriptRoot)\GlobalConfig.ps1" 
   exit 1 
}
. $GlobalFile

Write-Log "connecting to snapcreator server $scserver"
$snapcreator = Connect-ScServer -Name $scserver -Port $scport -Credential $sccred
if (!$snapcreator) {
	Write-Log "ERROR:could not connect to snapcreator server"
	$host.SetShouldExit(1) 
	exit 1
}

$v = Get-ScVolume -ProfileName $profile -ConfigName $config -OutVariable vols

if (-not @($vols).Count) {
	Write-Log "ERROR: at least one volume should be set in the SnapCreator configuration"
	$host.SetShouldExit(1) 
	exit 1
}


$conn = $false
$vol = $false
$svm = $false
$snap = $false

$vols | Foreach-Object {	
	if ($svm -and $_.Storage -ne $svm) {
		Write-Log "ERROR: only one SVM is supported for cloning (in the configuration there is at least 2: $($svm) and $($_.Storage)"
		$host.SetShouldExit(1) 
		exit 1		
	}
	
	$svm = $_.Storage 
	$vol = $_.Name

	if (-not $conn) {
		Write-Log "connecting to SVM $svm"
		$conn = Connect-NcController -Name $svm -Credential $cred 
	}
	
	Write-Log "validating that base volume exists $($svm):$($vol)"
	$voldetails = Get-NcVol -Controller $conn -Name $vol 
	if (!$voldetails) {
		Write-Log "ERROR: could not locate clone volume $($svm):$($vol)"
		$host.SetShouldExit(1) 
		exit 1		
	}
	Write-Log "$($svm):$($vol) validated"
	
	$fullclonename = $clonename+$vol
	Write-Log "validating that clone volume exists $($svm):$($fullclonename)"
	$clonedetails = Get-NcVol -Controller $conn -Name $fullclonename
	if (!$clonedetails) {
		Write-Log "ERROR: could not locate clone volume $($svm):$($fullclonename)"
		$host.SetShouldExit(1) 
		exit 1		
	}
	$volcomment = $clonedetails.VolumeIdAttributes.Comment
	$comments = $volcomment.Split(',')
	$comments | ForEach {
		$comment = $_
		$param,$value = $comment.Split(':') 
		if ($param -eq 'SNAP') {
			$snapshot = $value
		}
		if ($param -eq 'SCCLONENAME') {
			$origclonename = $value
		}	
	}
	if (!$snapshot -or !$origclonename) {
		Write-Log "ERROR:could not find required information on $($svm):$($fullclonename) comment field"
		$host.SetShouldExit(1) 
		exit 1
	}
	Write-Log "$($svm):$($fullclonename) validated"
	
	Write-Log "validating $($svm):$($vol) is replicated"
	$snapmirrordests = Get-NcSnapmirrorDestination -Source "$($svm):$($vol)" -Controller $conn
			
	if (-not $snapmirrordests ) {
		Write-Log "ERROR: $($svm):$($vol) doesn't have snapmirror destinations"
		$host.SetShouldExit(1) 
		exit 1		
	} 
	
	if ($(($snapmirrordests | measure).Count) -gt 1) {
		Write-Log "ERROR: $($svm):$($vol) have more than 1 destination"
		$host.SetShouldExit(1) 
		exit 1		
	}

	$destsvm = $snapmirrordests[0].DestinationVserver
	$destvol = $snapmirrordests[0].DestinationVolume
	
	Write-Log "$($destsvm):$($destvol) validated to be replicated to $($destsvm):$($destvol)"
	
	$fullclonename = $clonename+$vol
	Write-Log "validating that deatination clone volume $($destsvm):$($fullclonename) exists"
	if (-not $conn_dst) {
		$conn_dst = Connect-NcController -Name $destsvm -Credential $cred 
	}
	$clonedetails = Get-NcVol -Controller $conn_dst -Name $fullclonename
	if (!$clonedetails) {
		Write-Log "ERROR: could not locate clone volume $($svm):$($fullclonename)"
		$host.SetShouldExit(1) 
		exit 1		
	}
	$volcomment = $clonedetails.VolumeIdAttributes.Comment
	$comments = $volcomment.Split(',')
	$comments | ForEach {
		$comment = $_
		$param,$value = $comment.Split(':') 
		if ($param -eq 'SNAP') {
			$snapshot = $value
		}
		if ($param -eq 'SCCLONENAME') {
			$origclonename = $value
		}	
	}
	if (!$snapshot -or !$origclonename) {
		Write-Log "ERROR:could not find required information on $($svm):$($fullclonename) comment field"
		$host.SetShouldExit(1) 
		exit 1
	}
	Write-Log "$($svm):$($fullclonename) validated"


	Write-Log "validating clone $($svm):$($fullclonename) is not replicated"
	$snapmirrordests = Get-NcSnapmirrorDestination -Source "$($svm):$($fullclonename)" -Controller $conn
			
	if ($snapmirrordests) {
		$destsvm = $snapmirrordests[0].DestinationVserver
		$destvol = $snapmirrordests[0].DestinationVolume	
		Write-Log "ERROR: clone $($svm):$($fullclonename) is already replicated to $($destsvm):$($destvol)"
		$host.SetShouldExit(1) 
		exit 1		
	} 	
	
	$snapmirror = Get-NcSnapmirror -Source "$($svm):$($fullclonename)" -Controller $conn_dst
	if ($snapmirror) {
		$destsvm = $snapmirror[0].DestinationVserver
		$destvol = $snapmirror[0].DestinationVolume	
		Write-Log "ERROR: clone $($svm):$($fullclonename) is already replicated to $($destsvm):$($destvol)"
		$host.SetShouldExit(1) 
		exit 1		
	} 	
}


$vols | Foreach-Object {
	$svm = $_.Storage 
	$vol = $_.Name
	$snapmirrordests = Get-NcSnapmirrorDestination -Source "$($svm):$($vol)" -Controller $conn
	$destsvm = $snapmirrordests[0].DestinationVserver
	$destvol = $snapmirrordests[0].DestinationVolume
	
	$src_fullclonename = $clonename+$vol
	$dst_fullclonename = $clonename+$destvol
	
	$src = "$($svm):$($src_fullclonename)"
	$dst = "$($destsvm):$($dst_fullclonename)"
	
	Write-Log "creating snapmirror relationship between $src to $dst with schedule:$($schedule)"

	$out = New-NcSnapmirror -Source $src -Destination $dst -Schedule $schedule -Controller $conn_dst -ErrorVariable err
	if ($err) {
		Write-Log "ERROR: snapmirror creation failed. $($err.Exception)"
		$host.SetShouldExit(1) 
		exit 1			
	}
	$sm = Get-NcSnapmirror -Destination $dst -Controller $conn_dst -ErrorVariable err

	if (-not $sm) {
		Write-Log "ERROR: cannot get details about snapmirror relationship"
		$host.SetShouldExit(1) 
		exit 1		
	}
	
	if ($sm.MirrorState -eq 'broken-off') {
		Write-Log "resyncing relationship $src to $dst"
		$out = Invoke-NcSnapmirrorResync -Destination $dst -ErrorVariable err 
		if ($err) {
			Write-Log "ERROR: snapmirror resync failed. $($err.Exception)"
			$host.SetShouldExit(1) 
			exit 1			
		}
		$state = 'broken-off'
		while ($state -ne 'snapmirrored') {
			$sm = Get-NcSnapmirror -Destination $dst -Controller $conn_dst -ErrorVariable err
			$state = $sm.MirrorState
			Write-Log "State:$($state) Status:$($sm.status)"
		}
		Write-Log "resynced successfully"
	}
	
}


Write-Log "all clone snapmirror relationship created successfully "
$host.SetShouldExit(0)
exit 